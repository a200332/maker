import 'package:flutter/material.dart';

/** 获取屏幕宽度 */
double getScreenWidth(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .width;
}

/** 获取屏幕高度 */
double getScreenHeight(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .height;
}

/** 获取系统状态栏高度 */
double getSysStatsHeight(BuildContext context) {
  return MediaQuery
      .of(context)
      .padding
      .top;
}
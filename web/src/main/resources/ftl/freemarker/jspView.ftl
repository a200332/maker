${r'<#'}assign ctx=springMacroRequestContext.contextPath />
<${r'#'}include "/macro/publicMacro.ftl">
<${r'#'}import "/macro/pagination.ftl" as Pagination>

<${r'@'}header title="${util.comment(table)}简介">
    <link href="${r'<${'}ctx}/resource/common/css/pagination.css" rel="stylesheet">
    <script src="${r'<${'}ctx}/resource/common/js/pagination.js"></script>
</${r'@'}header>

<${r'@'}body>
<h2 class="module-title">${util.comment(table)}详情</h2>

<div class="alert alert-primary" role="alert">
    <button class="btn btn-info" onclick="javascript: history.back(-1);return false;" >返回</button>
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
            <th width="100">名称</th>
            <th>值</th>
        </thead>
        <tbody>
        <#list table.columnList as column>
            <tr>
                <td class="alert alert-info" align="center" align="center">${util.comment(column)}</td>
                <#if column.foreign>
                <td>${r'${'}${util.firstLower(table.javaName)}.${column.property}.${column.foreignKey.foreignTable.columnList[0].property}${r'!}'}</td>
                <#elseif util.isDate(column)>
                <td>${r'${('}${util.firstLower(table.javaName)}.${column.property}?string('yyyy-MM-dd hh:mm:ss')${r')!}'} </td>
                <#elseif util.isNumber(column)>
                <td>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</td>
                <#elseif util.isEnum(column)>
                <td>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</td>
                <#else>
                <td>${r'${'}${util.firstLower(table.javaName)}.${column.property}${r'!}'}</td>
                </#if>
            </tr>
        </#list>
        </tbody>
    </table>
</div>
</${r'@'}body>

<${r'@'}footer>

</${r'@'}footer>
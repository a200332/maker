security:
  oauth2:
    client:
      client-id: sso-demo
      client-secret: sso-demo-1234
      user-authorization-uri: http://118.190.60.69/oauth/authorize
      access-token-uri: http://118.190.60.69/oauth/token
      scope: all
  resource:
    jwt:
      key-uri: http://118.190.60.69/oauth/token_key
    user-info-uri: http://118.190.60.69/userInfo

spring:
  profiles:
    active: test

  application:
    name: ${projectName}

  datasource:
    name: hikariDataSource
    type: com.zaxxer.hikari.HikariDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    username: sa
    password:
    hikari:
      minimum-idle: 1
      maximum-pool-size: 3
      auto-commit: true
      idle-timeout: 30000
      pool-name: DatebookHikariCP
      max-lifetime: 1800000
      connection-timeout: 30000
      connection-test-query: SELECT 1

  freemarker:
    template-loader-path:
      - classpath:/templates
      - classpath:/ftl
    allow-request-override: false
    cache: false
    check-template-location: true
    charset: utf-8
    content-type: text/html
    expose-request-attributes: false
    expose-session-attributes: false
    expose-spring-macro-helpers: true
    request-context-attribute: request

server:
  port: 8282

management:
  endpoint:
    health:
      show-details: always
    shutdown:
      enabled: false
    env:
      enabled: false
  endpoints:
    web:
      exposure:
        include: health,info,metrics,threaddump,heapdump
        exclude: shutdown,env
    base-path: /actuator

swagger2:
  enable: true
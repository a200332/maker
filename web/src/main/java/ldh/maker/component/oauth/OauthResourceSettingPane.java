package ldh.maker.component.oauth;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.component.FreemarkerTableUi;
import ldh.maker.component.SettingPane;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class OauthResourceSettingPane extends SettingPane {

    public OauthResourceSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);
    }

}

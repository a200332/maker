package ldh.maker;

import ldh.maker.component.FreemarkerContentUiFactory;
import ldh.maker.component.oauth.FreemarkerOauthContentUiFactory;
import ldh.maker.util.UiUtil;

public class FreemarkerOauthWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new FreemarkerOauthContentUiFactory());
        UiUtil.setType("freemarkerOauth");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring cloud oauth2 + freemarker代码 ";
    }

    public static void main(String[] args) throws Exception {
        throw new RuntimeException("请运行MainLauncher");
//        startDb(null);
//        launch(args);
    }
}


spring.security.basic.enabled: true
spring.security.user.name = admin
spring.security.user.password=admin
server.port= 8761

eureka.instance.hostname= localhost
eureka.client.registerWithEureka=false
eureka.client.fetchRegistry=false

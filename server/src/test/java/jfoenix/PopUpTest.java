package jfoenix;

import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXPopup;
import com.jfoenix.controls.JFXRippler;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Created by ldh on 2019/2/12.
 */
public class PopUpTest extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane root = new StackPane();
        JFXListView<String> list = new JFXListView<String>();
        for(int i = 1 ; i < 5 ; i++) list.getItems().add("Item " + i);

        JFXHamburger burger = new JFXHamburger();
        burger.setPadding(new Insets(10,5,10,5));
        JFXRippler popupSource = new JFXRippler(burger, JFXRippler.RipplerMask.CIRCLE, JFXRippler.RipplerPos.BACK);

        JFXPopup popup = new JFXPopup();
        popup.setPopupContent(list);
//        popup.setPopupContainer(root);
//        popup.setSource(popupSource);
        popupSource.setOnMouseClicked((e)-> popup.show(root, JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.LEFT));
        root.getChildren().add(popupSource);
        primaryStage.setScene(new Scene(root, 200, 300));
        primaryStage.show();
    }
}
